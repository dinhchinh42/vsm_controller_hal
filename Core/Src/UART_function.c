#include "UART_function.h"

void USARTx_TX_Char(USART_TypeDef* USARTx, char data)
{
	/* Transmit one char and wairt until compeate */

	while((READ_BIT(USARTx->ISR, USART_ISR_TXE)==0)
	&&(READ_BIT(USARTx->ISR, USART_ISR_TC)==0));
	CLEAR_BIT(USARTx->ICR, USART_ICR_TCCF);
	USARTx->TDR = (data & (uint16_t)0x01FF);
}

/******************************************************************************/
void USARTx_TX_Cir(USART_TypeDef* USARTx, uint8_t *data, uint16_t *Tail_Ptr, uint16_t Buffer_size)
{
	/* Transmit one char and wairt until compeate with input param is a pointer */

	while((READ_BIT(USARTx->ISR, USART_ISR_TXE)==0)
	&&(READ_BIT(USARTx->ISR, USART_ISR_TC)==0));
	CLEAR_BIT(USARTx->ICR, USART_ICR_TCCF);
	USARTx->TDR = *data;
	*Tail_Ptr = (*Tail_Ptr + 1)% Buffer_size;
}

/******************************************************************************/
void USARTx_TX(USART_TypeDef* USARTx, char data)
{
	/* Transmit one char */

	while(!READ_BIT(USARTx->ISR, USART_ISR_TXE));
	USARTx->TDR = data;
}

/******************************************************************************/
void USARTx_TX_Str(USART_TypeDef* USARTx, char *string)
{
	/* Transmit one string and wairt until compleate */

	while(*string)
	USARTx_TX(USARTx, *string++);
	while(!READ_BIT(USARTx->ISR, USART_ISR_TC));
	SET_BIT(USARTx->ICR, USART_ICR_TCCF);
}

/******************************************************************************/
uint16_t USARTx_RX_CheckData(USART_TypeDef* USARTx)
{
	/* Cheack receiver byte, if one of error occur
	cancel receiver byte and transmit "!" to warning*/

	if((USART2->ICR&0xFFFFFFF0)!=0)
	{
		USART2->ISR &= 0xFFFFFFF0;
		return RX_fault;
	}
	else
		return USART2->RDR;
}

/******************************************************************************/
void USARTx_TX_IntNumber(USART_TypeDef* USARTx, unsigned char *Start_add)
{
	USARTx_TX_Char(USARTx, *Start_add);
	USARTx_TX_Char(USARTx, *(Start_add+1));
}

/******************************************************************************/
void USARTx_TX_FloatNumber(USART_TypeDef* USARTx, unsigned char *Start_add)
{
	USARTx_TX_Char(USARTx, *Start_add);
	USARTx_TX_Char(USARTx, *(Start_add+1));
	USARTx_TX_Char(USARTx, *(Start_add+2));
	USARTx_TX_Char(USARTx, *(Start_add+3));
}
