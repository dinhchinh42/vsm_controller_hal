/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f3xx_it.h"
#include "Circuler_Buffer.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
Circuler_Buffer_type Buff;
volatile uint8_t Buff_RX[1];
unsigned int cnt_Mess = 0;
unsigned int cnt_Motor = 0;
extern unsigned int ADC_product_Dispense[3] = {0};
extern unsigned int cnt_Dropss_Disable = 0;
unsigned int cnt_Global = 0;
bool Task_Operation = false;
static uint8_t volatile Buff_Array[CBuffer1_Size];
unsigned int ADC_result;
extern int mask_cnt = 0;
extern char mask = 0;

extern unsigned int phase = 0;
extern unsigned int phase_1 = 0;
extern unsigned int phase_2 = 0;
extern unsigned int phase_3 = 0;
extern unsigned int phase_4 = 0;

unsigned int timeout = 0;

Task task[16];
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/******************************************************************************/
void GPIOx_bit_toggle(GPIO_TypeDef* GPIOx, uint32_t bit)
{
  if(READ_BIT(GPIOx->ODR, bit))
	{
		 CLEAR_BIT(GPIOx->ODR, bit);
	}
	else
	{
		 SET_BIT(GPIOx->ODR, bit);
	}
}

void Task_init(Task *task)
{
	task[task_Diagnose].Task_active = false;
	task[task_Diagnose].Task_status = task_status_stop;
	task[task_Diagnose].Task_timeout = 5000;

	task[task_SlotReset].Task_active = false;
	task[task_SlotReset].Task_status = task_status_stop;
	task[task_SlotReset].Task_timeout = 5000;

	task[task_ReadTemp].Task_active = false;
	task[task_ReadTemp].Task_status = task_status_stop;
	task[task_ReadTemp].Task_timeout = 5000;

	task[task_Comb_One_Single].Task_active = false;
	task[task_Comb_One_Single].Task_status = task_status_stop;
	task[task_Comb_One_Single].Task_timeout = 5000;

	task[task_Comb_One_Double].Task_active = false;
	task[task_Comb_One_Double].Task_status = task_status_stop;
	task[task_Comb_One_Double].Task_timeout = 5000;

	task[task_Comb_all_Single].Task_active = false;
	task[task_Comb_all_Single].Task_status = task_status_stop;
	task[task_Comb_all_Single].Task_timeout = 5000;

	task[task_Settemp].Task_active = false;
	task[task_Settemp].Task_status = task_status_stop;
	task[task_Settemp].Task_timeout = 5000;

	task[task_Settime_Defrost].Task_active = false;
	task[task_Settime_Defrost].Task_status = task_status_stop;
	task[task_Settime_Defrost].Task_timeout = 5000;

	task[task_Settime_Working].Task_active = false;
	task[task_Settime_Working].Task_status = task_status_stop;
	task[task_Settime_Working].Task_timeout = 5000;

	task[task_Settime_Down].Task_active = false;
	task[task_Settime_Down].Task_status = task_status_stop;
	task[task_Settime_Down].Task_timeout = 5000;

	task[task_Light].Task_active = false;
	task[task_Light].Task_status = task_status_stop;
	task[task_Light].Task_timeout = 5000;

	task[task_Mode_OneBelt].Task_active = false;
	task[task_Mode_OneBelt].Task_status = task_status_stop;
	task[task_Mode_OneBelt].Task_timeout = 5000;

	task[task_Mode_OneSpring].Task_active = false;
	task[task_Mode_OneSpring].Task_status = task_status_stop;
	task[task_Mode_OneSpring].Task_timeout = 5000;

	task[task_Mode_AllBelt].Task_active = false;
	task[task_Mode_AllBelt].Task_status = task_status_stop;
	task[task_Mode_AllBelt].Task_timeout = 5000;

	task[task_Mode_AllSpring].Task_active = false;
	task[task_Mode_AllSpring].Task_status = task_status_stop;
	task[task_Mode_AllSpring].Task_timeout = 5000;

	task[task_Product_Dispense].Task_active = false;
	task[task_Product_Dispense].Task_status = task_status_stop;
	task[task_Product_Dispense].Task_timeout = 5000;
}
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM6_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_ADC2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// Add by Toan - Control freezer
int dem = 0;
int dem1 = 0;
int check_hex = 0;
int check_light = 0;
// Add by Toan - Control freezer

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
 if(htim->Instance == htim6.Instance)
 {

	 //TIM_ClearITPendingBit(TIM6 , TIM_IT_Update);
	 cnt_Global++;

	 dem++;
	 dem1++;
	 check_hex++;
	 check_light++;

	 if(cnt_Dropss_Disable)
	 	cnt_Dropss_Disable++;

	 if(Task_Operation == true)
	 	 timeout++;

	 if(mask == 1)
	 mask_cnt++;


	 if(phase == 1)
	 {
	 	phase_1++;
	 }

	 if(phase == 2)
	 {
	 	phase_2++;
	 }

	 if(phase == 3)
	 {
	 	phase_3++;
	 }

	 if(phase == 4)
	 {
	 	phase_4++;
	 }
 }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)    //data: 8byte
{
	if(huart -> Instance == USART2)
	{
		Buff_RX[0] = USART2->RDR; //lay du lieu tu thanh ghi rdr luu vao buff_rx
//		for(int i=0;i<=0;i++)
//		{
		Circular_Buffer_Put(&Buff, Buff_RX[0]);
		//}
		HAL_UART_Receive_IT(&huart2, (uint8_t*)Buff_RX, sizeof(Buff_RX));
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if(hadc->Instance == hadc2.Instance)
	 {
		static char cnt_MotorCurrent = 0;
		char Arr[7];
		ADC_product_Dispense[cnt_MotorCurrent] = HAL_ADC_GetValue(&hadc2);
		cnt_MotorCurrent++;
		if(cnt_MotorCurrent > 2)
			cnt_MotorCurrent = 0;
	 }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_0)
	{
		if(READ_BIT(EXTI->PR, EXTI_PR_PR0))
		{
			SET_BIT(EXTI->PR, EXTI_PR_PR0);
			CLEAR_BIT(EXTI->EMR, EXTI_EMR_MR0);
			if(cnt_Dropss_Disable == 0)
				cnt_Dropss_Disable++;
		}
	}
}

/**********************************************************************************************/
bool check_Time(unsigned int *cnt_local, unsigned int cnt_global, unsigned int thresholed)
{
	unsigned int result;

	if(cnt_global >= * cnt_local)
		result = cnt_global - *cnt_local;
	else
		result = *cnt_local - cnt_global;

	if(result > thresholed)
	{
		*cnt_local = cnt_global;
		return true;
	}
	else
		return false;
}
/**********************************************************************************************/
bool check_TaskActive(Task *task, char begin)
{
	bool havetask;

	if(begin == 0x01)
	{
		for(int i = 0; i < 16; i++)
		{
			if(task[i].Task_active == true)
			{
				havetask = false;
				break;
			}
			else
				havetask = true;
		}
	}
	else if(begin == 0x02)
	{
		for(int i = 0; i < 16; i++)
		{
			if(task[i].Task_active == true)
			{
				havetask = true;
				break;
			}
			else
				havetask = false;
		}
	}
	else if(begin == 0x03)
	{
		for(int i = 0; i < 16; i++)
		{
			if(task[i].Task_status == task_status_doing)
			{
				havetask = true;
				break;
			}
			else
				havetask = false;
		}
	}

	return havetask;
}

/**********************************************************************************************/
bool check_Mottor_Toggle(unsigned int ADC_value, unsigned int th_down)
{
	bool result = false;
	static char godown_cnt = 0;

	if(ADC_value < th_down)
	{
		godown_cnt++;
		if(godown_cnt > 3)
		{
			godown_cnt = 0;
			result = true;
		}
	}
	else
		godown_cnt = 0;

	return result;
}

/**********************************************************************************************/
void Con2And_send(unsigned char *and2con, unsigned char Serial, unsigned char Fault, unsigned char Code, unsigned char Dispense, bool drop)
{
	and2con[rep_Serial] = Serial;
	and2con[rep_Fault] = Fault;
	and2con[rep_Code] = Code;
	if(drop)
		and2con[rep_Dispense] = Dispense;
	else
		and2con[rep_Dispense] = 0x00;

	and2con[rep_Checksum] = and2con[rep_Serial] + and2con[rep_Fault] + and2con[rep_Code] + and2con[rep_Dispense];

	for(int i = rep_Serial; i <= rep_Checksum; i++)
	{
		USARTx_TX_Char(USART2, and2con[i]);
		and2con[i] = 0;
	}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM6_Init();
  MX_USART2_UART_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  /* USER CODE BEGIN 2 */
  char Data;
  static unsigned char Sequence = 0;
  static unsigned char cmd_MesDone = 0;  //bien co kiem tra de biet la da doc het lenh chua
  unsigned char And2Con[8]; //mang de chua 8 byte gui tu android len controller
  unsigned char Con2And[5]; //mang de chua 5 byte phan hoi tu controller ve android
  unsigned int Slot[10] = {SLOT0_Pin, SLOT1_Pin, SLOT2_Pin, SLOT3_Pin, SLOT4_Pin, SLOT5_Pin, SLOT6_Pin, SLOT7_Pin, SLOT8_Pin, SLOT9_Pin}; //10 slot moi tray
  GPIO_TypeDef * Slot_P[10] = {SLOT0_GPIO_Port, SLOT1_GPIO_Port, SLOT2_GPIO_Port, SLOT3_GPIO_Port, SLOT4_GPIO_Port, SLOT5_GPIO_Port, SLOT6_GPIO_Port, SLOT7_GPIO_Port, SLOT8_GPIO_Port, SLOT9_GPIO_Port};
  unsigned int Trace[6] = {TRACE0_Pin, TRACE1_Pin, TRACE2_Pin, TRACE3_Pin, TRACE4_Pin, TRACE5_Pin}; //tong 6 tray
  GPIO_TypeDef * Trace_P[6] = {TRACE0_GPIO_Port, TRACE1_GPIO_Port, TRACE2_GPIO_Port, TRACE3_GPIO_Port, TRACE4_GPIO_Port, TRACE5_GPIO_Port};
  static char Dropss = dropss_nodrop;
  unsigned int slot;
  unsigned int trace;

  HAL_UART_Receive_IT(&huart2, (uint8_t *)Buff_RX, sizeof(Buff_RX));
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_ADC_Start_IT(&hadc2);

  GPIO_InitTypeDef GPIO_led_init;

//  HAL_GPIO_DeInit(GPIOC, GPIO_PIN_All);
//  HAL_GPIO_DeInit(GPIOB, GPIO_PIN_All);
  //HAL_GPIO_DeInit(GPIOA, GPIO_PIN_All);

 /* GPIO Periph clock enable */

  //GPIO_Init(GPIOA, &GPIO_led_init);

  HAL_GPIO_WritePin(Trace_P[0], Trace[0], 0);

  /* Basic function config */
  SysTick_init();
  /* Peripheral function config */
  Circular_Buff_Init(&Buff, Buff_Array, CBuffer1_Size);
  Task_init(task);

  // Add by Toan - Control freezer
  	char Display[20];
  	extern int dem;
  	extern int dem1;
  	extern int check_hex;
  	extern int check_light;

  	int Status_check = 0;
  	int time_active_avai = 5;  // chu ky dinh san bo lanh chay theo phut
  	int time_rest_avai = 5;
  	int time_active_require = 5;  // chu ky yeu cau bo lạnh chay theo phut
  	int time_rest_require = 5;

  	int time_temp_active = 10; // chu ky dinh san khi chay  theo phut
  	int time_temp_rest = 10;
  	int temp_require;

  	int active = 0;
  	float sum = 0;
  	double sum1 = 0;
  	double Voltage_1 = 0;
  	double Voltage_2 = 0;
  	double Vol_R0 = 0;
  	double Divide_R0 = 0;
  	double Rx = 0;
  	double A_NTC = 1.009249522e-03;
  	double B_NTC = 2.378405444e-04;
  	double C_NTC = 2.019202697e-07;
  	double T1 = 0;
  	double T2 = 0;
  	char Temp_C = 0;
  	// Add by Toan - Control freezer

  	for(char i = 10; i > 0 ; i--)
  	{
  		GPIOx_bit_toggle(GPIOF, GPIO_PIN_1);
  		//GPIOx_bit_toggle(GPIOA, GPIO_PIN_5);
  		HAL_Delay(200);
  	}
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	/* For check drop sensor 2 */
	static unsigned int count_on = 0;
	static unsigned int count_off = 0;
	static unsigned char count_drop = 0;

	static unsigned int cnt_led = 0;

	if(check_Time(&cnt_led, cnt_Global, CNT_LED_TH))
	{
		GPIOx_bit_toggle(GPIOF, GPIO_PIN_1);
		//GPIOx_bit_toggle(GPIOA, GPIO_PIN_5);
		//USARTx_TX_Str(USART2, "Drop sensor\r\n");
	}

	/**********************************************************************************************/
	#ifdef DROP_CHECK_MULTI
	/* Check Drop sensor */
	if(READ_BIT(GPIOA->IDR, GPIO_IDR_0) == 0)
	{
		if(cnt_Dropss_Disable > 5)
		{
			cnt_Dropss_Disable = 0;
			EXTI->IMR |= EXTI_IMR_MR0;
			if(task[task_Product_Dispense].Task_status == task_status_doing)
			{
				Dropss = dropss_valid;
			}
			else if(task[task_Product_Dispense].Task_status == task_status_stop)
			{
				Dropss = dropss_invalid;
				#ifdef DEMO
				static unsigned int cnt_dropss  = 0;
				if(check_Time(&cnt_dropss, cnt_Global, 500))
					USARTx_TX_Str(USART2, "Drop sensor invalid\r\n");
				#else
				static bool first_time = false;
				static unsigned int cnt_dropss  = 0;
				Con2And[rep_Serial] = 0x00;
				Con2And[rep_Fault] = REP_B1_FAIL;
				Con2And[rep_Code] = REP_B1_FAIL;
				Con2And[rep_Dispense] = REP_DROP_WHENNON;
				Con2And[rep_Checksum] = Con2And[rep_Serial] + Con2And[rep_Fault] + Con2And[rep_Code] + Con2And[rep_Dispense];

				if(first_time == false)
				{
					for(int i = rep_Serial; i <= rep_Checksum; i++)
						USARTx_TX_Char(USART2, Con2And[i]);
					first_time = true;
				}
				else
				{
					if(check_Time(&cnt_dropss, cnt_Global, 2000))
					{
						for(int i = rep_Serial; i <= rep_Checksum; i++)
							USARTx_TX_Char(USART2, Con2And[i]);
						first_time = false;
					}
				}
				#endif
			}
		}
	}
	#else
	/* Check Drop sensor 2 */
	if(READ_BIT(GPIOA->IDR, GPIO_IDR_0) == 0)
	{
		if(check_Time(&count_on, cnt_Global, 1000))
		{
			if(task[task_Product_Dispense].Task_status == task_status_doing)
			{
				//Dropss = dropss_valid;
				count_drop++;
			}
			else if(task[task_Product_Dispense].Task_status == task_status_stop)
			{
				Dropss = dropss_invalid;
				count_drop = 0;
				//USARTx_TX_Str(USART2, "Dropss invalid - drop when non\r\n");
			}
		}
	}
	else
	{
		if(check_Time(&count_off, cnt_Global, 50))
		{
			count_drop = 0;
		}
	}

	//Count number of time sensor detect
	if(count_drop == 1)
	{
		static unsigned int count = 0;
		Dropss = dropss_valid;

		if(check_Time(&count, cnt_Global, 500));
			//USARTx_TX_Str(USART2, "Dropss valid\r\n");
	}
	else if(count_drop > 1)
	{
		static unsigned int count = 0;
		Dropss = dropss_invalid;

		if(check_Time(&count, cnt_Global, 500));
			//USARTx_TX_Str(USART2, "Dropss invalid - drop multi\r\n");
	}
	#endif

	/**********************************************************************************************/
	/* Check Message valid or not */
	if(check_hex == 10 && Status_check == 1)
	{
		for(int i = cmd_Start; i < cmd_Stop; i++)
		{
			And2Con[i] = 0;
		}
		Sequence = cmd_Start;
		check_hex = 0;
		Status_check = 0;
	}

	if(Circular_Buffer_Get(&Buff, &Data) && (!cmd_MesDone)) //lay du lieu tu buff gan vao data (! la khac 1, cmd_MesDone = 0)
	{
		switch (Sequence)
		{
			case cmd_Start:
				if(Data == CMD_START)
				{
					And2Con[Sequence] = Data;
					Sequence = cmd_Serial;
					//HAL_UART_Transmit(&huart2, "ok1", 3, 100);
				}
				check_hex = 0;
				Status_check = 1;
				break;

			case cmd_Serial:
				And2Con[Sequence] = Data;
				Sequence = cmd_Serial_buf;
				Status_check = 1;
				//HAL_UART_Transmit(&huart2, "ok2", 3, 100);
				break;

			case cmd_Serial_buf:
				And2Con[Sequence] = Data;
				Sequence = cmd_Code1;
				Status_check = 1;
				//HAL_UART_Transmit(&huart2, "ok3", 3, 100);
				break;

			case cmd_Code1:
				And2Con[Sequence] = Data;
				Sequence = cmd_Code1_buf;
				Status_check = 1;
				//HAL_UART_Transmit(&huart2, "ok4", 3, 100);
				break;

			case cmd_Code1_buf:
				And2Con[Sequence] = Data;
				Sequence = cmd_Code2;
				Status_check = 1;
				//HAL_UART_Transmit(&huart2, "ok5", 3, 100);
				break;

			case cmd_Code2:
				And2Con[Sequence] = Data;
				Sequence = cmd_Code2_buf;
				Status_check = 1;
				//HAL_UART_Transmit(&huart2, "ok6", 3, 100);
				break;

			case cmd_Code2_buf:
				And2Con[Sequence] = Data;
				Sequence = cmd_Stop;
				Status_check = 1;
				//HAL_UART_Transmit(&huart2, "ok7", 3, 100);
				break;

			case cmd_Stop:
				if (Data == CMD_STOP) {
					//HAL_UART_Transmit(&huart2, "ok8", 3, 100);
					And2Con[Sequence] = Data;
					//Checksum, check xem moi byte canh nhau co bang 0xFF khong, neu bang thi cmd_MesDone = 1 de doi bien va tiep tuc doc
					if (((And2Con[cmd_Serial] + And2Con[cmd_Serial_buf]) == 255)&&
					   ((And2Con[cmd_Code1] + And2Con[cmd_Code1_buf]) == 255)&&
					   ((And2Con[cmd_Code2] + And2Con[cmd_Code2_buf]) == 255))
					{
						cmd_MesDone = 1;  //kiem tra la ma gui da dung

					}
					Status_check = 0;
				}
				else //neu truyen loi (0 nhan dc stop bit la 0x55) thi reset tat cac byte ve 0
				{
					for (int i = cmd_Start; i < cmd_Stop; i++)
					And2Con[i] = 0;
				}
				Sequence = cmd_Start; //reset chuoi lenh sequence = 0
				break;
			}
	}

	/**********************************************************************************************/
	/* Classify message */
	#ifdef TEST_RX
	if(cmd_MesDone)
	{
		cmd_MesDone = 0;
		USARTx_TX_Str(USART2, "Mess is:\r\n");

		//Check serial number
		if(And2Con[cmd_Serial] == 0)
			USARTx_TX_Str(USART2, "Serial number correct\r\n");
		else
			USARTx_TX_Str(USART2, "Serial number incorrect\r\n");

		//Check if is product dispense
		if((0x01 <= And2Con[cmd_Code1]) && (And2Con[cmd_Code1] <= 0x50))
		{
			char Arr[5];
			sprintf(Arr, "%d", And2Con[cmd_Code1]);
			USARTx_TX_Str(USART2, "CODE1 - Product dispense: ");
			USARTx_TX_Str(USART2, Arr);
			USARTx_TX_Str(USART2, "\r\n");
			if(And2Con[cmd_Code2] == 0x55)
				USARTx_TX_Str(USART2, "CODE2 - Without drop sensor function\r\n");
			else if(And2Con[cmd_Code2] == 0xAA)
				USARTx_TX_Str(USART2, "CODE2 - With drop sensor function\r\n");
			else
				USARTx_TX_Str(USART2, "CODE2 - Error\r\n");
		}

		//Check if is other mess
		else
		{
			//Check code1
			switch (And2Con[cmd_Code1])
			{
				//SELF DIAGNOSE
				case CMD_DIAGNOSE:
				{
					USARTx_TX_Str(USART2, "CODE1 - Driver dianose\r\n");
					if(And2Con[cmd_Code2] == CMD2_DEFAULT)
						USARTx_TX_Str(USART2, "CODE2 - Default\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent\r\n");
					break;
				}

				//ALL SLOT RESET
				case CMD_SLOTRESET:
				{
					USARTx_TX_Str(USART2, "CODE1 - Slot reset\r\n");
					if(And2Con[cmd_Code2] == CMD2_DEFAULT)
						USARTx_TX_Str(USART2, "CODE2 - Default\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent\r\n");
					break;
				}

				//READ TEMPERATURE
				case CMD_READTEMP:
				{
					USARTx_TX_Str(USART2, "CODE1 - Read Temperature\r\n");
					if(And2Con[cmd_Code2] == CMD2_DEFAULT)
						USARTx_TX_Str(USART2, "CODE2 - Default\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent\r\n");
					break;
				}

				//SET ONE SOT IS SINGLE SLOT
				case CMD_COMB_ONE_SINGLE:
				{
					USARTx_TX_Str(USART2, "CODE1 - setup one slot is single\r\n");
					if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50))
						USARTx_TX_Str(USART2, "CODE2 - incorrent slot number\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set slot %d is single mode\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET ONE SLOT IS DOUBLE SLOT
				case CMD_COMB_ONE_DOUBLE:
				{
					USARTx_TX_Str(USART2, "CODE1 - setup one slot is double\r\n");
					if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50))
						USARTx_TX_Str(USART2, "CODE2 - incorrent slot number");
					else
					{
						char Arr[50];
						sprintf(Arr, "Set slot %d is double mode\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET ALL SLOT IS SINGLE SLOT
				case CMD_COMB_ALL_SINGLE:
				{
					USARTx_TX_Str(USART2, "CODE1 - setup all slot is single\r\n");
					if(And2Con[cmd_Code2] == CMD2_DEFAULT)
						USARTx_TX_Str(USART2, "CODE2 - Default\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent\r\n");
					break;
				}

				//SET TEMPERATURE
				case CMD_SETTEMP:
				{
					USARTx_TX_Str(USART2, "CODE1 - Set temperature\r\n");
					if(And2Con[cmd_Code2] > 10)
						USARTx_TX_Str(USART2, "CODE2 - incorrent temperature\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set temperature to %d degree\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET DEFROST TIME
				case CMD_SETTIME_DEFROST:
				{
					USARTx_TX_Str(USART2, "CODE1 - Set time defrost\r\n");
					if(And2Con[cmd_Code2] > 50)
						USARTx_TX_Str(USART2, "CODE2 - incorrent defrost time\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set defrost time is %d minute\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET WORKING TIME
				case CMD_SETTIME_WORKING:
				{
					USARTx_TX_Str(USART2, "CODE1 - Set time working\r\n");
					if(And2Con[cmd_Code2] > 20)
						USARTx_TX_Str(USART2, "CODE2 - incorrent working time\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set working time is %d minute\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET DOWN TIME
				case CMD_SETTIME_DOWN:
				{
					USARTx_TX_Str(USART2, "CODE1 - Set time Down\r\n");
					if(And2Con[cmd_Code2] > 50)
						USARTx_TX_Str(USART2, "CODE2 - incorrent down time\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set down time is %d minute\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//OPEN CLOSE LED LIGHT
				case CMD_LIGHT:
				{
					USARTx_TX_Str(USART2, "CODE1 - Setup light led\r\n");
					if(And2Con[cmd_Code2] == CMD2_LIGHT_ON)
						USARTx_TX_Str(USART2, "CODE2 - Turn on light led\r\n");
					else if(And2Con[cmd_Code2] == CMD2_LIGHT_OFF)
						USARTx_TX_Str(USART2, "CODE2 - Turn off light led\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent led control\r\n");
					break;
				}

				//SET ONE SLOT IS BELT MODE
				case CMD_MODE_ONE_BELT:
				{
					USARTx_TX_Str(USART2, "CODE1 - Setup one slot is belt\r\n");
					if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50))
						USARTx_TX_Str(USART2, "CODE2 - incorrent slot number\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set slot %d is belt mode\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET ONE SLOT IS SPRING MODE
				case CMD_MODE_ONE_SPRING:
				{
					USARTx_TX_Str(USART2, "CODE1 - Setup one slot is spring\r\n");
					if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50))
						USARTx_TX_Str(USART2, "CODE2 - incorrent slot number\r\n");
					else
					{
						char Arr[50];
						sprintf(Arr, "CODE2 - Set slot %d is spring mode\r\n", And2Con[cmd_Code2]);
						USARTx_TX_Str(USART2, Arr);
					}
					break;
				}

				//SET ALL SLOT IS BELT MODE
				case CMD_MODE_ALL_BELT:
				{
					USARTx_TX_Str(USART2, "CODE1 - Setup all slot is belt\r\n");
					if(And2Con[cmd_Code2] == CMD2_DEFAULT)
						USARTx_TX_Str(USART2, "CODE2 - Default\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent\r\n");
					break;
				}

				//SET ALL SLOT IS SPRING MODE
				case CMD_MODE_ALL_SPRING:
				{
					USARTx_TX_Str(USART2, "CODE1 - Setup all slot is spring\r\n");
					if(And2Con[cmd_Code2] == CMD2_DEFAULT)
						USARTx_TX_Str(USART2, "CODE2 - Default\r\n");
					else
						USARTx_TX_Str(USART2, "CODE2 - incorrent\r\n");
					break;
				}

				//Code 1 error
				default:
					USARTx_TX_Str(USART2, "CODE1 - Error\r\n");
			}
		}
	}

	else
	{
		if(check_Time(&cnt_Mess, CNT_MESS_TH))
			USARTx_TX_Str(USART2, "Dont have mess or mess fail\r\n");
	}

	#else  //check ma
	if(cmd_MesDone && (Task_Operation == false))
	{
		cmd_MesDone = 0;
		//Check serial number
		if(And2Con[cmd_Serial] == 0)  //kiem tra so seri cua bo mach controller, neu =0 thi tra ve la 0
		{
			Con2And[rep_Serial] = 0x00;
			//HAL_UART_Transmit(&huart2, "okSeri", 6, 100);
		}
		else //neu khac 0 thi bao loi
		{
			//Con2And[rep_Serial] = 0x00;
			Con2And[rep_Fault] = REP_B1_FAIL;
		}

		//Check code1 if not product dispense
		switch (And2Con[cmd_Code1])
		{
			//SELF DIAGNOSE
			case CMD_DIAGNOSE:
			{
				if(And2Con[cmd_Code2] == CMD2_DEFAULT){
					//task[task_Diagnose].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Diagnose].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok1", 3, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//ALL SLOT RESET
			case CMD_SLOTRESET:
			{
				if(And2Con[cmd_Code2] == CMD2_DEFAULT){
					//task[task_SlotReset].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_SlotReset].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok2", 3, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//READ TEMPERATURE
			case CMD_READTEMP:
			{
				if(And2Con[cmd_Code2] == CMD2_DEFAULT){
//					task[task_ReadTemp].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_ReadTemp].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok3", 3, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET ONE SOT IS SINGLE SLOT
			case CMD_COMB_ONE_SINGLE:
			{
				if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50)){
					//task[task_Comb_One_Single].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Comb_One_Single].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok4", 3, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET ONE SLOT IS DOUBLE SLOT
			case CMD_COMB_ONE_DOUBLE:
			{
				if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50)){
//					task[task_Comb_One_Double].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Comb_One_Double].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok5", 3, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET ALL SLOT IS SINGLE SLOT
			case CMD_COMB_ALL_SINGLE:
			{
				if(And2Con[cmd_Code2] == CMD2_DEFAULT){
//					task[task_Comb_all_Single].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Comb_all_Single].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok6", 3, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET TEMPERATURE
			case CMD_SETTEMP:
			{
				if(And2Con[cmd_Code2] > 10){
					Con2And[rep_Fault] = REP_B1_FAIL;
					//HAL_UART_Transmit(&huart2, "ok7", 3, 100);
				}
				else
					//task[task_Settemp].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Settemp].Task_active = true;
				break;
			}

			//SET DEFROST TIME
			case CMD_SETTIME_DEFROST:
			{
				if(And2Con[cmd_Code2] > 50){
					Con2And[rep_Fault] = REP_B1_FAIL;
					//HAL_UART_Transmit(&huart2, "ok8", 3, 100);
				}
				else
//					task[task_Settime_Defrost].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Settime_Defrost].Task_active = true;
				break;
			}

			//SET WORKING TIME
			case CMD_SETTIME_WORKING:
			{
				if(And2Con[cmd_Code2] > 100){
					Con2And[rep_Fault] = REP_B1_FAIL;
					//HAL_UART_Transmit(&huart2, "ok9", 3, 100);
				}
				else
//					task[task_Settime_Working].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Settime_Working].Task_active = true;
				break;
			}

			//SET DOWN TIME
			case CMD_SETTIME_DOWN:
			{
				if(And2Con[cmd_Code2] > 50){
					Con2And[rep_Fault] = REP_B1_FAIL;
					//HAL_UART_Transmit(&huart2, "ok10", 4, 100);
				}
				else
//					task[task_Settime_Down].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Settime_Down].Task_active = true;
				break;
			}

			//OPEN CLOSE LED LIGHT
			case CMD_LIGHT:
			{
				//task[task_Light].Task_active = check_TaskActive(&task[16], 0x01);
				task[task_Light].Task_active = true;
				//HAL_UART_Transmit(&huart2, "ok11", 4, 100);
				break;
			}

			//SET ONE SLOT IS BELT MODE
			case CMD_MODE_ONE_BELT:
			{
				if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50)){
					task[task_Mode_OneBelt].Task_active = check_TaskActive(&task[16], 0x01);
					//HAL_UART_Transmit(&huart2, "ok12", 4, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET ONE SLOT IS SPRING MODE
			case CMD_MODE_ONE_SPRING:
			{
				if((And2Con[cmd_Code2] < 0x01) || (And2Con[cmd_Code2] > 0x50)){
					//task[task_Mode_OneSpring].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Mode_OneSpring].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok13", 4, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET ALL SLOT IS BELT MODE
			case CMD_MODE_ALL_BELT:
			{
				if(And2Con[cmd_Code2] == CMD2_DEFAULT){
					//task[task_Mode_AllBelt].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Mode_AllBelt].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok14", 4, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//SET ALL SLOT IS SPRING MODE
			case CMD_MODE_ALL_SPRING:
			{
				if(And2Con[cmd_Code2] == CMD2_DEFAULT){
					//task[task_Mode_AllSpring].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Mode_AllSpring].Task_active = true;
					//HAL_UART_Transmit(&huart2, "ok15", 4, 100);
				}
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
				break;
			}

			//Code 1 error
			default:
			{
				//Check if is product dispense
				if((0x00 <= And2Con[cmd_Code1]) && (And2Con[cmd_Code1]) <= 0x50)
				{
					if(And2Con[cmd_Code1] >= MACHINE_TRACE_SLOT)
					{
						trace = And2Con[cmd_Code1]/MACHINE_TRACE_SLOT;    //open motor
						slot = And2Con[cmd_Code1] - (trace*MACHINE_TRACE_SLOT);
					}
					else
					{
						trace = 0;
						slot = And2Con[cmd_Code1];
					}
					//task[task_Product_Dispense].Task_active = check_TaskActive(&task[16], 0x01);
					task[task_Product_Dispense].Task_active = true;
				}
				//Code 1 error
				else
					Con2And[rep_Fault] = REP_B1_FAIL;
			}
		}
		if(Con2And[rep_Fault] == REP_B1_FAIL)
		Con2And_send(&Con2And[5], 0x00, REP_B1_FAIL, 0x00, REP_DROP_NORMAL, false);
	}
	#endif

	/**********************************************************************************************/
	/* Task operation */
	if(((Task_Operation == false) && check_TaskActive(&task[16], 0x02)) || check_TaskActive(&task[16], 0x03))
	{

		/*--------------------------------------------------------------------------------*/
		/* Product dispense */
		if(task[task_Product_Dispense].Task_active)
		{
			Task_Operation = true;
			static bool NoDrop = false;
			static char ghim = 0;

			/* task doing */
			if(task[task_Product_Dispense].Task_status == task_status_doing)
			{
				#ifdef DEMO
				static unsigned int cnt_PD = 0;
				if(check_Time(&cnt_PD, cnt_Global, 1000))
				{
					char Arr1[4];
					char Arr2[4];
					sprintf(Arr1, "%d", trace);
					sprintf(Arr2, "%d", slot);

					USARTx_TX_Str(USART2, "\r\nTrace: ");
					USARTx_TX_Str(USART2, Arr1);
					USARTx_TX_Str(USART2, "\r\n");
					USARTx_TX_Str(USART2, "Slot: ");
					USARTx_TX_Str(USART2, Arr2);
					USARTx_TX_Str(USART2, "\r\n");

					USARTx_TX_Str(USART2, "Prodcut dispense\r\n");
					USARTx_TX_Str(USART2, "Prodcut dispense task doing\r\n");

					if(Dropss == dropss_valid)
						USARTx_TX_Str(USART2, "Drop sensor detected\r\n");
					else if(Dropss == dropss_nodrop)
						USARTx_TX_Str(USART2, "Drop sensor still don't detect\r\n");
				}
				#else
				static unsigned char Motor_toggle = 0;

				if(ghim == 0)
				{
					HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 1);
					HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 1);

					HAL_ADC_Start(&hadc2);
					if(mask == 0)
						mask = 1;
					//while(!ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC));
					ADC_result = HAL_ADC_GetValue(&hadc2);
				}

				//Check motor start phase
				if(mask == 1)
				{
					if(ADC_result > 4095)
					{
						HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);
						HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);
					}
					else if(mask_cnt > 400)
					{
						mask_cnt = 0;
						mask = 2;
					}
				}

				//Check motor posotion stop
				else if(((mask == 2) && check_Mottor_Toggle(ADC_result, 200)) || (ghim == 1))
				{
					if(ghim == 0)
						Motor_toggle++;

					if((Motor_toggle >= 1) && (ghim == 0))
					{
						Motor_toggle = 0;

						HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);
						HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);
						//USARTx_TX_Str(USART2, "Slot: ");
						if((Dropss == dropss_nodrop) && (ghim == 0))
							ghim = 1;
					}

					if(ghim == 1)
					{
						#ifdef SMALL_LCD
						if(phase == 0)
							phase = 1;

						switch(phase)
						{
							case 1:
								if(phase_1 >= 500)
								{
									phase_1 = 0;
									phase = 2;
									HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 1);
									HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 1);
								}
								break;

							case 2:
								if(phase_2 >= 200)
								{
									phase_2 = 0;
									phase = 3;
									HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);
									HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);
								}
								break;

							case 3:
								if(phase_3 >= 500)
								{
									phase_3 = 0;
									phase = 4;
									HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 1);
									HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 1);
								}
								break;

							case 4:
								if(phase_4 >= 200)
								{
									phase_4 = 0;
									phase = 0;
									ghim = 0;
									HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);
									HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);

									unsigned char drop = 0;
									if(Dropss == dropss_valid)
										drop = REP_DROP_NORMAL;
									else
										drop = REP_DROP_NON;

									Con2And_send(&Con2And, 0x00, REP_B1_NORMAL, 0x00, drop, true);

									mask = 0;
									timeout = 0;
									task[task_Product_Dispense].Task_status = task_status_stop;
									task[task_Product_Dispense].Task_active = false;
									Task_Operation = false;
								}
								break;
						}
						#else
						ghim = 0;
						unsigned char drop = 0;
						if(Dropss == dropss_valid)
							drop = REP_DROP_NORMAL;
						else
							drop = REP_DROP_NON;

						Con2And_send(&Con2And[5], 0x00, REP_B1_NORMAL, 0x00, drop, true);

						mask = 0;
						timeout = 0;
						task[task_Product_Dispense].Task_status = task_status_stop;
						task[task_Product_Dispense].Task_active = false;
						Task_Operation = false;
						#endif
					}
				}

				//Check motor shorst circuit and motor open circuit
				else if(mask == 2)
				{
					//Check motor shorst circuit
					if(ADC_result > 4095)
					{
						HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);
						HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);

						unsigned char drop = 0;
						if(Dropss == dropss_valid)
							drop = REP_DROP_NORMAL;
						else
							drop = REP_DROP_NON;
						USARTx_TX(USART2, 0x33);
						Con2And_send(&Con2And[5], 0x00, REP_B1_FAIL, REP_MOTOR_SHORT, 0x00, false);

						mask = 0;
						timeout = 0;
						task[task_Product_Dispense].Task_status = task_status_stop;
						task[task_Product_Dispense].Task_active = false;
						Task_Operation = false;
					}
					//Check motor open circuit
					else if(ADC_result < 100)
					{
						HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);
						HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);

						unsigned char drop = 0;
						if(Dropss == dropss_valid)
							drop = REP_DROP_NORMAL;
						else
							drop = REP_DROP_NON;

						Con2And_send(&Con2And[5], 0x00, REP_B1_FAIL, REP_MOTOR_OPEN, 0x00, false);

						mask = 0;
						timeout = 0;
						task[task_Product_Dispense].Task_status = task_status_stop;
						task[task_Product_Dispense].Task_active = false;
						Task_Operation = false;
					}
				}
				#endif

				/* Check task timeout */
				if(timeout > task[task_Product_Dispense].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Prodcut dispense task timeout!!!!\r\n");
					if(Dropss == dropss_nodrop)
						USARTx_TX_Str(USART2, "Error: Drop sensor don't detect item\r\n");
					#else

					HAL_GPIO_WritePin(Trace_P[trace], Trace[trace], 0);
					HAL_GPIO_WritePin(Slot_P[slot], Slot[slot], 0);

					unsigned char drop = 0;
					if(Dropss == dropss_valid)
						drop = REP_DROP_NORMAL;
					else
						drop = REP_DROP_NON;

					Con2And_send(&Con2And[5], 0x00, REP_B1_FAIL, REP_MOTOR_TIMEOUT, drop, true);
					#endif

					mask = 0;
					timeout = 0;
					task[task_Product_Dispense].Task_status = task_status_stop;
					task[task_Product_Dispense].Task_active = false;
					Task_Operation = false;
				}
			}

			/* if task not doing preveus, start task */
			else if(task[task_Product_Dispense].Task_status == task_status_stop)
			{
				task[task_Product_Dispense].Task_status = task_status_doing;
				Dropss = dropss_nodrop;
			}
		}

		/*--------------------------------------------------------------------------------*/
		/* Diagnose */
		else if(task[task_Diagnose].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Diagnose].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_Diagnose = 0;
				if(check_Time(&cnt_Diagnose, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task diaglose\r\n");
					USARTx_TX_Str(USART2, "Diaglose task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Diagnose].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Diaglose task  task timeout\r\n");
					#endif

					timeout = 0;
					task[task_Diagnose].Task_status = task_status_stop;
					task[task_Diagnose].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Diagnose].Task_status == task_status_stop)
				task[task_Diagnose].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* SlotReset */
		else if(task[task_SlotReset].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_SlotReset].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_Slotreset = 0;
				if(check_Time(&cnt_Slotreset, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Slotreset\r\n");
					USARTx_TX_Str(USART2, "Slotreset task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_SlotReset].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Slotreset task timeout\r\n");
					#endif

					timeout = 0;
					task[task_SlotReset].Task_status = task_status_stop;
					task[task_SlotReset].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_SlotReset].Task_status == task_status_stop)
				task[task_SlotReset].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* ReadTemp */
		else if(task[task_ReadTemp].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_ReadTemp].Task_status == task_status_doing)
			{
				static unsigned long sum = 0;
				#ifdef DEMO
				static unsigned int cnt_Led = 0;
				if(check_Time(&cnt_Led, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Readtemp\r\n");
					USARTx_TX_Str(USART2, "Readtemp task doing\r\n");
				}
				#else
				static unsigned char cnt_temp_ADC = 0;

				cnt_temp_ADC++;

				if(cnt_temp_ADC > 50)
				{
					cnt_temp_ADC = 0;

//						char Arr[6];
//						sprintf(Arr,"%d",Temp_C);
//						USARTx_TX_Str(USART2, Arr);
//						USARTx_TX_Str(USART2, "\r\n");

					if((Temp_C < 1) || (Temp_C > 100))
						Con2And_send(&Con2And[5], 0x00, REP_B1_FAIL, 0x00, 0x00, false);
					else
						Con2And_send(&Con2And[5], 0x00, REP_B1_NORMAL, Temp_C, 0x00, false);

					timeout = 0;
					task[task_ReadTemp].Task_status = task_status_stop;
					task[task_ReadTemp].Task_active = false;
					Task_Operation = false;
				}
				#endif

				/* Check task timeout */
				if(timeout > task[task_ReadTemp].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Readtemp task timeout!!!\r\n");
					#endif

					Con2And_send(&Con2And[5], 0x00, REP_B1_FAIL, REP_MOTOR_TIMEOUT, 0x00, false);

					timeout = 0;
					task[task_ReadTemp].Task_status = task_status_stop;
					task[task_ReadTemp].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_ReadTemp].Task_status == task_status_stop)
				task[task_ReadTemp].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup one slot is single */
		else if(task[task_Comb_One_Single].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Comb_One_Single].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_COS = 0;
				if(check_Time(&cnt_COS, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Setup one slot is single\r\n");
					USARTx_TX_Str(USART2, "Setup one slot is single task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Comb_One_Single].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Setup one slot is single task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Comb_One_Single].Task_status = task_status_stop;
					task[task_Comb_One_Single].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Comb_One_Single].Task_status == task_status_stop)
				task[task_Comb_One_Single].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup one slot is double */
		else if(task[task_Comb_One_Double].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Comb_One_Double].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_COD = 0;
				if(check_Time(&cnt_COD, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Setup one slot is Double\r\n");
					USARTx_TX_Str(USART2, "Setup one slot is Double task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Comb_One_Double].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Setup one slot is single task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Comb_One_Double].Task_status = task_status_stop;
					task[task_Comb_One_Double].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Comb_One_Double].Task_status == task_status_stop)
				task[task_Comb_One_Double].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup all slot is single */
		else if(task[task_Comb_all_Single].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Comb_all_Single].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_CAS  = 0;
				if(check_Time(&cnt_CAS, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Setup all slot is single\r\n");
					USARTx_TX_Str(USART2, "Setup all slot is single task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Comb_all_Single].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Setup all slot is single task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Comb_all_Single].Task_status = task_status_stop;
					task[task_Comb_all_Single].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Comb_all_Single].Task_status == task_status_stop)
				task[task_Comb_all_Single].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup temperature */
		else if(task[task_Settemp].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Settemp].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_Settemp = 0;
				if(check_Time(&cnt_Settemp, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Set temperature\r\n");
					USARTx_TX_Str(USART2, "Set temperature task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Settemp].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Set temperature task timeout!!!\r\n");
					active = 1;
					temp_require = And2Con[cmd_Code2];
					dem = 0;
					#endif

					Con2And_send(&Con2And[5], 0x00, REP_MOS_NORMAL, 0x00, 0x00, false);

					temp_require = And2Con[cmd_Code2];
					active  = 1;
					dem = 0;
					timeout = 0;
					task[task_Settemp].Task_status = task_status_stop;
					task[task_Settemp].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Settemp].Task_status == task_status_stop)
				task[task_Settemp].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup defrost time */
		else if(task[task_Settime_Defrost].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Settime_Defrost].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_SD = 0;
				if(check_Time(&cnt_SD, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Set time defrost\r\n");
					USARTx_TX_Str(USART2, "Set time defrost task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Settime_Defrost].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Set time defrost task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Settime_Defrost].Task_status = task_status_stop;
					task[task_Settime_Defrost].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Settime_Defrost].Task_status == task_status_stop)
				task[task_Settime_Defrost].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup working time */
		else if(task[task_Settime_Working].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Settime_Working].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_SW = 0;
				if(check_Time(&cnt_SW, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Setup working time\r\n");
					USARTx_TX_Str(USART2, "Setup working time task doing\r\n");
				}
				#else

				#endif

				/* Check task timeout */
				if(timeout > task[task_Settime_Working].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Setup working time task timeout!!!\r\n");
					time_active_require = And2Con[cmd_Code2];
					#else
					time_active_require = And2Con[cmd_Code2];
					Con2And_send(&Con2And[5], 0x00, REP_MOS_NORMAL, 0x00, 0x00, false);
					#endif

					timeout = 0;
					task[task_Settime_Working].Task_status = task_status_stop;
					task[task_Settime_Working].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Settime_Working].Task_status == task_status_stop)
				task[task_Settime_Working].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup down time */
		else if(task[task_Settime_Down].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Settime_Down].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_ST = 0;
				if(check_Time(&cnt_ST, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task Setup down time\r\n");
					USARTx_TX_Str(USART2, "Setup down time task doing\r\n");
				}
				#else

				#endif

				/* Check task timeout */
				if(timeout > task[task_Settime_Down].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Setup down time timeout!!!\r\n");
					time_rest_require = And2Con[cmd_Code2];
					#else
					time_rest_require = And2Con[cmd_Code2];
					Con2And_send(&Con2And[5], 0x00, REP_MOS_NORMAL, 0x00, 0x00, false);
					#endif

					timeout = 0;
					task[task_Settime_Down].Task_status = task_status_stop;
					task[task_Settime_Down].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Settime_Down].Task_status == task_status_stop)
				task[task_Settime_Down].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup light */
		else if(task[task_Light].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Light].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_Light = 0;
				if(check_Time(&cnt_Light, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task on off light\r\n");
					USARTx_TX_Str(USART2, "On off light task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Light].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "On off light task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Light].Task_status = task_status_stop;
					task[task_Light].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Light].Task_status == task_status_stop)
				task[task_Light].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup one slot is belt mode */
		else if(task[task_Mode_OneBelt].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Mode_OneBelt].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_MOB = 0;
				if(check_Time(&cnt_MOB, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task on off light\r\n");
					USARTx_TX_Str(USART2, "On off light task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Mode_OneBelt].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "On off light task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Mode_OneBelt].Task_status = task_status_stop;
					task[task_Mode_OneBelt].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Mode_OneBelt].Task_status == task_status_stop)
				task[task_Mode_OneBelt].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup one slot is spring mode */
		else if(task[task_Mode_OneSpring].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Mode_OneSpring].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned cnt_MOS = 0;
				if(check_Time(&cnt_MOS, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task set one slot spring\r\n");
					USARTx_TX_Str(USART2, "Set one slot spring task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Mode_OneSpring].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Set one slot spring task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Mode_OneSpring].Task_status = task_status_stop;
					task[task_Mode_OneSpring].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Mode_OneSpring].Task_status == task_status_stop)
				task[task_Mode_OneSpring].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup all slot is belt mode */
		else if(task[task_Mode_AllBelt].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Mode_AllBelt].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_MAB = 0;
				if(check_Time(&cnt_MAB, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task set all slot belt\r\n");
					USARTx_TX_Str(USART2, "Set all slot belt task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Mode_AllBelt].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Set all slot belt task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Mode_AllBelt].Task_status = task_status_stop;
					task[task_Mode_AllBelt].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Mode_AllBelt].Task_status == task_status_stop)
				task[task_Mode_AllBelt].Task_status = task_status_doing;
		}

		/*--------------------------------------------------------------------------------*/
		/* Setup all slot is spring mode */
		else if(task[task_Mode_AllSpring].Task_active)
		{
			Task_Operation = true;

			/* task doing */
			if(task[task_Mode_AllSpring].Task_status == task_status_doing)
			{

				#ifdef DEMO
				static unsigned int cnt_MAS = 0;
				if(check_Time(&cnt_MAS, cnt_Global, 1000))
				{
					USARTx_TX_Str(USART2, "Task set all slot spring\r\n");
					USARTx_TX_Str(USART2, "Set all slot spring task doing\r\n");
				}
				#else
				#endif

				/* Check task timeout */
				if(timeout > task[task_Mode_AllSpring].Task_timeout)
				{
					#ifdef DEMO
					USARTx_TX_Str(USART2, "Set all slot spring task timeout!!!\r\n");
					#endif

					timeout = 0;
					task[task_Mode_AllSpring].Task_status = task_status_stop;
					task[task_Mode_AllSpring].Task_active = false;
					Task_Operation = false;
				}
			}
			/* if task not doing preveus, start task */
			else if(task[task_Mode_AllSpring].Task_status == task_status_stop)
				task[task_Mode_AllSpring].Task_status = task_status_doing;
		}
	}

	/*--------------------------------------------------------------------------------*/
	// Bat quat
	HAL_GPIO_WritePin(GPIOC, FAN_Pin, 1);
	if(check_light == 5*1000)
	{
		HAL_GPIO_WritePin(GPIOB, LIGHT_Pin, 1);
	}
	if(check_light == 10*1000)
	{
		HAL_GPIO_WritePin(GPIOB, LIGHT_Pin, 0);
		check_light = 0;
	}
//	/*--------------------------------------------------------------------------------*/
//	// PT100 - Toan
	if(dem1 == 1*1000)
	{
		for(int i = 0; i < 200; i++)
		{
			HAL_ADC_Start(&hadc1);
			float value_adc1 = HAL_ADC_GetValue(&hadc1);
			//while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
			sum += value_adc1;
		}
		sum1 = (float) sum/200;
		Voltage_1 = (float)sum1 * 3.3;
		Voltage_2 = (float)Voltage_1/4096;
		/********/
		Vol_R0 = (float)5 - Voltage_2;
		Divide_R0 = (float)Vol_R0 / 10000;
		Rx = (float)Voltage_2 / Divide_R0;
		T1 = A_NTC + B_NTC*log(Rx)+ C_NTC*log(Rx)*log(Rx)*log(Rx);
		T2 = 1 / T1;
		Temp_C = T2 - 273.15;
		/********/
		sprintf(Display,"Temp_NTC:%d\n",Temp_C);
		//USARTx_TX_Str(USART2,Display);
		sum = 0;
		dem1 = 0;
	}
//
//	// Add by Toan - Control freezer
	if(time_active_require != time_active_avai)
	{
		time_active_avai = time_active_require;
		time_temp_active = time_active_require;
		//USARTx_TX_Str(USART2,"Da chinh sua thoi gian bat\n");
	}

	if(time_rest_require != time_rest_avai)
	{
		time_rest_avai = time_rest_require;
		time_temp_rest = time_rest_require;
		//USARTx_TX_Str(USART2,"Da chinh sua thoi gian nghi\n");
	}

	if(active == 0)
	{
		if(dem == 1)
		{ // tat may lanh
			HAL_GPIO_WritePin(GPIOC, COOL_Pin, 0);
			//USARTx_TX_Str(USART2, "Chu ky - Tat may lanh\n");
		}
		if(dem == time_rest_avai * 60 * 1000)
		{ // bat may lanh
			HAL_GPIO_WritePin(GPIOC, COOL_Pin, 1);
			//USARTx_TX_Str(USART2, "Chu ky - Bat may lanh\n");
		}
		if(dem >= (time_rest_avai + time_active_avai)* 60 * 1000)
		{
			dem = 0;
		}
	}
//
//	if(active == 1)
//	{
//		if(temp_require < Temp_C)
//		{
//			if(dem == 1*1000)
//			{
//				HAL_GPIO_WritePin(GPIOC, COOL_Pin, 1);   // bat may lanh
//				//USARTx_TX_Str(USART2, "Nho hon - Bat may lanh\n");
//			}
//			if(dem == time_temp_active * 60 * 1000)
//			{
//				HAL_GPIO_WritePin(GPIOC, COOL_Pin, 0);
//				//USARTx_TX_Str(USART2, "Nho hon - Tat may lanh\n");
//			}
//			if(dem >= ((time_temp_active + time_temp_rest)* 60 * 1000))
//			{
//				dem = 0;
//			}
//		}
//		if(temp_require > Temp_C)
//		{
//			HAL_GPIO_WritePin(GPIOC, COOL_Pin, 0); // tat may lanh
////				if ( dem == 2 * 1000){
////					//USARTx_TX_Str(USART2, "Lon hon -  Tat may lanh\n");
////					dem = 0;
////				}
//		}
//		if((Temp_C - 1 <= temp_require) && (temp_require <= Temp_C + 1))
//		{
//			active = 0;
//				//USARTx_TX_Str(USART2, "Tro ve ban dau");
//		}
//	}
	// Add by Toan - Control freezer


	#ifdef TEST_MOTOR
	//GPIOx_bit_toggle(GPIOF, GPIO_Pin_1);

	if(check_Time(&cnt_Motor, 5000))
		GPIO_ResetBits(GPIOA, TRACE1);
	//Delay_ms(5000);

	if(check_Time(&cnt_Motor, 5000))
		GPIO_SetBits(GPIOA, TRACE1);
	//Delay_ms(5000);
	#endif
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_12;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_61CYCLES_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_AnalogWDGConfTypeDef AnalogWDGConfig = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Common config
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analog WatchDog 1
  */
  AnalogWDGConfig.WatchdogNumber = ADC_ANALOGWATCHDOG_1;
  AnalogWDGConfig.WatchdogMode = ADC_ANALOGWATCHDOG_SINGLE_REG;
  AnalogWDGConfig.HighThreshold = 4095;
  AnalogWDGConfig.LowThreshold = 0;
  AnalogWDGConfig.Channel = ADC_CHANNEL_1;
  AnalogWDGConfig.ITMode = DISABLE;
  if (HAL_ADC_AnalogWDGConfig(&hadc2, &AnalogWDGConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 63;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 999;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, HEAT_Pin|COOL_Pin|FAN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, DOOR_Pin|LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, DROPSS_OUT_Pin|GPIO_PIN_5|SLOT4_Pin|SLOT3_Pin
                          |SLOT2_Pin|SLOT1_Pin|SLOT0_Pin|TRACE0_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LIGHT_Pin|DOOR_HEAT_Pin|SLOT9_Pin|SLOT8_Pin
                          |SLOT7_Pin|SLOT6_Pin|SLOT5_Pin|TRACE1_Pin
                          |TRACE2_Pin|TRACE3_Pin|TRACE4_Pin|TRACE5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : HEAT_Pin COOL_Pin FAN_Pin */
  GPIO_InitStruct.Pin = HEAT_Pin|COOL_Pin|FAN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : DOOR_Pin LED_Pin */
  GPIO_InitStruct.Pin = DOOR_Pin|LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pin : DROPSS_IN_Pin */
  GPIO_InitStruct.Pin = DROPSS_IN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(DROPSS_IN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DROPSS_OUT_Pin PA5 SLOT4_Pin SLOT3_Pin
                           SLOT2_Pin SLOT1_Pin SLOT0_Pin TRACE0_Pin */
  GPIO_InitStruct.Pin = DROPSS_OUT_Pin|GPIO_PIN_5|SLOT4_Pin|SLOT3_Pin
                          |SLOT2_Pin|SLOT1_Pin|SLOT0_Pin|TRACE0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LIGHT_Pin DOOR_HEAT_Pin SLOT9_Pin SLOT8_Pin
                           SLOT7_Pin SLOT6_Pin SLOT5_Pin TRACE1_Pin
                           TRACE2_Pin TRACE3_Pin TRACE4_Pin TRACE5_Pin */
  GPIO_InitStruct.Pin = LIGHT_Pin|DOOR_HEAT_Pin|SLOT9_Pin|SLOT8_Pin
                          |SLOT7_Pin|SLOT6_Pin|SLOT5_Pin|TRACE1_Pin
                          |TRACE2_Pin|TRACE3_Pin|TRACE4_Pin|TRACE5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
