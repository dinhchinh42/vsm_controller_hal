/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "SysTick_Delay.h"
#include "UART_function.h"
#include "Circuler_Buffer.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
//#define TEST_RX
//#define TEST_MOTOR
//#define DEMO
//#define SMALL_LCD
//#define DROP_CHECK_MULTI
#define START 0x01
#define STOP 0x09
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define HEAT_Pin GPIO_PIN_13
#define HEAT_GPIO_Port GPIOC
#define COOL_Pin GPIO_PIN_14
#define COOL_GPIO_Port GPIOC
#define FAN_Pin GPIO_PIN_15
#define FAN_GPIO_Port GPIOC
#define DOOR_Pin GPIO_PIN_0
#define DOOR_GPIO_Port GPIOF
#define LED_Pin GPIO_PIN_1
#define LED_GPIO_Port GPIOF
#define DROPSS_IN_Pin GPIO_PIN_0
#define DROPSS_IN_GPIO_Port GPIOA
#define DROPSS_OUT_Pin GPIO_PIN_1
#define DROPSS_OUT_GPIO_Port GPIOA
#define UART2_TX_Pin GPIO_PIN_2
#define UART2_TX_GPIO_Port GPIOA
#define UART2_RX_Pin GPIO_PIN_3
#define UART2_RX_GPIO_Port GPIOA
#define LIGHT_Pin GPIO_PIN_2
#define LIGHT_GPIO_Port GPIOB
#define DOOR_HEAT_Pin GPIO_PIN_10
#define DOOR_HEAT_GPIO_Port GPIOB
#define SLOT9_Pin GPIO_PIN_11
#define SLOT9_GPIO_Port GPIOB
#define SLOT8_Pin GPIO_PIN_12
#define SLOT8_GPIO_Port GPIOB
#define SLOT7_Pin GPIO_PIN_13
#define SLOT7_GPIO_Port GPIOB
#define SLOT6_Pin GPIO_PIN_14
#define SLOT6_GPIO_Port GPIOB
#define SLOT5_Pin GPIO_PIN_15
#define SLOT5_GPIO_Port GPIOB
#define SLOT4_Pin GPIO_PIN_8
#define SLOT4_GPIO_Port GPIOA
#define SLOT3_Pin GPIO_PIN_9
#define SLOT3_GPIO_Port GPIOA
#define SLOT2_Pin GPIO_PIN_10
#define SLOT2_GPIO_Port GPIOA
#define SLOT1_Pin GPIO_PIN_11
#define SLOT1_GPIO_Port GPIOA
#define SLOT0_Pin GPIO_PIN_12
#define SLOT0_GPIO_Port GPIOA
#define TRACE0_Pin GPIO_PIN_15
#define TRACE0_GPIO_Port GPIOA
#define TRACE1_Pin GPIO_PIN_3
#define TRACE1_GPIO_Port GPIOB
#define TRACE2_Pin GPIO_PIN_4
#define TRACE2_GPIO_Port GPIOB
#define TRACE3_Pin GPIO_PIN_5
#define TRACE3_GPIO_Port GPIOB
#define TRACE4_Pin GPIO_PIN_6
#define TRACE4_GPIO_Port GPIOB
#define TRACE5_Pin GPIO_PIN_7
#define TRACE5_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define Max 1000
typedef struct task
{
	bool Task_active;
	char Task_status;
	unsigned int Task_timeout;
}Task;

enum dropss
{
	dropss_nodrop,
	dropss_valid,
	dropss_invalid
};

enum task_ID
{
	task_Diagnose,          //tu chuan doan tinh trang
	task_SlotReset,         //reset all slot
	task_ReadTemp,				 	//doc nhiet do
	task_Comb_One_Single,	  //cai dat mot slot nao do la slot don
	task_Comb_One_Double,   //cai dat moot slot nao do la slot doi
	task_Comb_all_Single,   //cai dat tat ca cac slot la slot don
	task_Settemp,           //cai dat nhiet do may
	task_Settime_Defrost,   //cai dat thoi gian pha bang
	task_Settime_Working,   //cai dat thoi gian lam lanh
	task_Settime_Down,			//cai dat thoi gian nghi
	task_Light,             //dieu khien bat tat den
	task_Mode_OneBelt,      //cai dat mot slot kieu bang tai
	task_Mode_OneSpring,    //cai dat mot slot kieu lo xo
	task_Mode_AllBelt,      //cai dat tat ca cac slot kieu belt
	task_Mode_AllSpring,		//cai dat tat ca cac slot kieu lo xo
	task_Product_Dispense   //NHA HANG
};

enum task_Status
{
	task_status_stop,     //task active but not doing
	task_status_doing,    //task active and doing
	task_status_error     //task have problem
};

#define TEST
#define CNT_LED_TH 1000
#define CNT_MESS_TH 1000

/* Exported macro ------------------------------------------------------------*/
#define led      GPIO_ODR_5
#define CBuffer1_Size 256

#define CMD_START (unsigned char)0xAA            //byte start
#define CMD_DIAGNOSE (unsigned char)0x64         //code de chuan doan tinh trang cac thiet bi
#define CMD_SLOTRESET (unsigned char)0x65        //reset tat ca cac slot
#define CMD_READTEMP (unsigned char)0xDC         //doc nhiet do tu
#define CMD_COMB_ONE_SINGLE (unsigned char)0xC9  //set mot slot la slot don
#define CMD_COMB_ONE_DOUBLE (unsigned char)0xCA  //set mot slot la slot doi
#define CMD_COMB_ALL_SINGLE (unsigned char)0xCB  //set tat ca cac slot la slot don
#define CMD_SETTEMP (unsigned char)0xCF          //cai dat nhiet do
#define CMD_SETTIME_DEFROST (unsigned char)0xD1  //cai dat thoi gian pha bang
#define CMD_SETTIME_WORKING (unsigned char)0xD2  //cai dat thoi gian chay cua may nen
#define CMD_SETTIME_DOWN (unsigned char)0xD3     //cai dat thoi gian nghi cua may nen
#define CMD_LIGHT (unsigned char)0xDD            //cai dat den led
#define CMD_MODE_ONE_BELT (unsigned char)0x68    //cai dat mot slot la kieu bang tai
#define CMD_MODE_ONE_SPRING (unsigned char)0x74  //cai dat mot slot la kieu lo xo
#define CMD_MODE_ALL_BELT (unsigned char)0x76    //cai dat tat ca cac slot la kieu bang tai
#define CMD_MODE_ALL_SPRING (unsigned char)0x75  //cai dat tat ca cac slot la kieu lo xo
#define CMD_STOP (unsigned char)0x55             //byte stop

#define CMD2_DISP_NOSENSOR (unsigned char)0x55
#define CMD2_DISP_SENSOR (unsigned char)0xAA
#define CMD2_LIGHT_ON (unsigned char)0xAA
#define CMD2_LIGHT_OFF (unsigned char)0x55
#define CMD2_DEFAULT (unsigned char)0x55

#define REP_B1_NORMAL 0x5D
#define REP_B1_FAIL 0x5C

#define REP_MOS_NORMAL 0x00
#define REP_PMOS_SHORT 0x10
#define REP_NMOS_SHORT 0x20
#define REP_MOTOR_SHORT 0x30
#define REP_MOTOR_OPEN 0x40
#define REP_MOTOR_TIMEOUT 0x50

#define REP_DROP_NORMAL 0x00
#define REP_DROP_WHENNON 0x01
#define REP_DROP_NON 0x02
#define REP_DROP_WHENMOTOR 0x03

#define MACHINE_TRACE 6
#define MACHINE_TRACE_SLOT 10
#define MACHINE_SLOT 60

#define ADC_ISR_PRIORITY 1
#define UART_ISR_PRIORITY 0
#define EXTI_ISR_PRIORITY 2
enum cmd_And2Con
{
	cmd_Start,
	cmd_Serial,
	cmd_Serial_buf,
	cmd_Code1,
	cmd_Code1_buf,
	cmd_Code2,
	cmd_Code2_buf,
	cmd_Stop
};

enum rep_Con2And
{
	rep_Serial,
	rep_Fault,
	rep_Code,
	rep_Dispense,
	rep_Checksum
};

/* "task_Product_Dispense" Task status */
enum product_Dispense_Status
{
	product_Dispense_relay1,
	product_Dispense_relay2
};


/* Private variables ---------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void GPIOx_bit_toggle(GPIO_TypeDef* GPIOx, uint32_t bit);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
